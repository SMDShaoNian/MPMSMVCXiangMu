﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Linq.Expressions;



namespace MemberPoint.Business
{
    public class BaseService<T> where T : class, new()
    {
        protected DbContext db = ContextFactory.GetCurrentContext();

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Add(T entity)
        {
            db.Entry<T>(entity).State = EntityState.Added;
            return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Delete(T entity)
        {
            db.Set<T>().Attach(entity);
            db.Entry<T>(entity).State = EntityState.Deleted;
            return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 更新实体
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Update(T entity)
        {
            db.Set<T>().Attach(entity);
            db.Entry<T>(entity).State = EntityState.Modified;
            return db.SaveChanges() > 0;
        }

        /// <summary>
        /// 查询实体
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public T Find(Expression<Func<T, bool>> whereLambda)
        {
            return db.Set<T>().FirstOrDefault(whereLambda);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="whereLambda"></param>
        /// <returns></returns>
        public IQueryable<T> GetList(Expression<Func<T, bool>> whereLambda)
        {
            return db.Set<T>().Where(whereLambda);
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="whereLambda"></param>
        /// <param name="orderby"></param>
        /// <param name="IsAsc"></param>
        /// <returns></returns>
        public IQueryable<T> GetList<TKey>(Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderby,bool IsAsc=true)
        {
            if (IsAsc == true)
            {
                return db.Set<T>().Where(whereLambda).OrderBy(orderby);
            }
            else
            {
                return db.Set<T>().Where(whereLambda).OrderByDescending(orderby);
            }
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <typeparam name="Tkey"></typeparam>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="rowCount"></param>
        /// <param name="whereLambda"></param>
        /// <param name="orderBy"></param>
        /// <param name="isASC"></param>
        /// <returns></returns>
        public IQueryable<T> GetListPage<Tkey>(int pageIndex, int pageSize, ref int rowCount, Expression<Func<T, bool>> whereLambda, Expression<Func<T, Tkey>> orderBy, bool isASC = true)
        {
            pageIndex = pageIndex - 1 < 0 ? 1 : pageIndex;
            rowCount = db.Set<T>().Where(whereLambda).Count();
            if (isASC)
            {
                return db.Set<T>().Where(whereLambda).OrderBy(orderBy).Skip(pageSize * (pageIndex - 1)).Take(pageSize);
            }

            else
            {
                return db.Set<T>().Where(whereLambda).OrderByDescending(orderBy).Skip(pageSize * (pageIndex - 1)).Take(pageSize);
            }

        }


        
        public IQueryable<T> GetList<TKey>(int pageIndex, int pageSize, ref int rowCount, Expression<Func<T, bool>> whereLambda, Expression<Func<T, TKey>> orderby, bool isAsc = true)
        {
            pageIndex = pageIndex - 1 < 0 ? 1 : pageIndex;
            rowCount = db.Set<T>().Where(whereLambda).Count();
            if (isAsc)
            {
                return db.Set<T>().Where(whereLambda).OrderBy(orderby).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            else
            {
                return db.Set<T>().Where(whereLambda).OrderByDescending(orderby).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }


        }





    }
}
