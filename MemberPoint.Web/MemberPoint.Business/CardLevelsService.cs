﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;



namespace MemberPoint.Business
{
    public class CardLevelsService : BaseService<CardLevels>
    {
        #region 会员等级下拉框的绑定
        public OpearteResult GetCardLevels()
        {
            var query = GetList(e => true).Select(e => new CardLevelsViewModel
            {
                CL_ID = e.CL_ID,
                CL_LevelName = e.CL_LevelName
            });
            return new OpearteResult(ResultStatus.Success, "", query);
        }
        #endregion

        /// <summary>
        /// 列表显示
        /// </summary>
        /// <returns></returns>
        public OpearteResult GetCardLevelsList(GetPageGetCardLevels getgage)
        {
            var list = PredicateExtensions.True<CardLevels>();
            int pageCount = 0;
            if (!string.IsNullOrEmpty(getgage.name))
            {
                list = list.And(e => e.CL_LevelName.Contains(getgage.name));
            }
            var query = GetListPage(getgage.page, getgage.rows, ref pageCount, list, e => e.CL_ID, true)
                .Select(e => new
                {
                    CL_ID = e.CL_ID,
                    CL_LevelName = e.CL_LevelName,
                    CL_NeedPoint = e.CL_NeedPoint,
                    CL_Point = e.CL_Point,
                    CL_Percent = e.CL_Percent

                }).ToList();

            var result = new { total = pageCount, rows = query };
            return new OpearteResult(ResultStatus.Success, "", result);

        }
        /// <summary>
        /// 新增会员卡类别
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public OpearteResult AddCardLevels(CardLevelsViewModel info)
        {
            var query = GetList(e => true).FirstOrDefault(e => e.CL_LevelName == info.CL_LevelName);
            if (query == null)
            {
                CardLevels model = new CardLevels();
                model.CL_LevelName = info.CL_LevelName;
                model.CL_NeedPoint = info.CL_NeedPoint;
                model.CL_Percent = info.CL_Percent;
                model.CL_Point = info.CL_Point;
                if (Add(model))
                {
                    return new OpearteResult(ResultStatus.Success, "添加成功", query);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "添加失败", query);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "等级名称已存在", query);
            }
        }
        /// <summary>
        /// 显示需要需改的信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CardLevelsViewModel ShowCardLevels(int id)
        {
            var query = Find(e => e.CL_ID == id);
            var list = new CardLevelsViewModel
            {
                CL_ID = query.CL_ID,
                CL_LevelName = query.CL_LevelName,
                CL_NeedPoint = query.CL_NeedPoint,
                CL_Percent = query.CL_Percent,
                CL_Point = query.CL_Point
            };
            return list;
        }
        /// <summary>
        /// 修改会员卡类别
        /// </summary>
        /// <param name="info"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult EditCardLevels(CardLevelsViewModel info, int id)
        {
            var query = Find(e => e.CL_ID == id);
            if (query != null)
            {
                query.CL_LevelName = info.CL_LevelName;
                query.CL_NeedPoint = info.CL_NeedPoint;
                query.CL_Percent = info.CL_Percent;
                query.CL_Point = info.CL_Point;
                if (Update(query))
                {
                    return new OpearteResult(ResultStatus.Success, "修改成功", query);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Success, "修改成功", query);
                }

            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "找不到该条信息", query);
            }
        }
        /// <summary>
        /// 删除会员卡类别
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult DeleteCardLevels(int id)
        {
            var query = Find(e => e.CL_ID == id);
            if (query != null)
            {
                if (Delete(query))
                {
                    return new OpearteResult(ResultStatus.Success, "删除成功", query);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "删除失败", query);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "找不到该条信息", query);
            }
        }
    }
}

