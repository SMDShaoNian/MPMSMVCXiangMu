﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Common;
using System.Data;
using MemberPoint.Entity.DTOModel;

namespace MemberPoint.Business
{
  public  class ConsumeOrdersService:BaseService<ConsumeOrders>
    {
        MemCardsService memCardsService = new MemCardsService();


        #region 消费历史记录列表
        /// <summary>
        /// 消费历史记录列表
        /// </summary>
        /// <param name="getUser"></param>
        /// <returns></returns>
        public OpearteResult GetConsumeOrdersList(GetPageConsumeOrdersViewModel getConsumeOrders)
        {

            var query = PredicateExtensions.True<ConsumeOrders>();
            if (!string.IsNullOrWhiteSpace(getConsumeOrders.MC_CardID))
            {
                query = query.And(e => e.MC_CardID.Contains(getConsumeOrders.MC_CardID));
            }

            int rowCount = 0;
            var pageDate = GetListPage(getConsumeOrders.page, getConsumeOrders.rows, ref rowCount, query, e => e.U_ID, true)
                .Select(e => new
                {
                    CO_ID = e.CO_ID,
                    S_ID = e.S_ID,
                    CO_OrderCode = e.CO_OrderCode,
                    CO_OrderType = e.CO_OrderType,
                    MC_CardID = e.MC_CardID,
                    MC_ID = e.MC_ID,
                    EG_ID = e.EG_ID,
                    CO_TotalMoney = e.CO_TotalMoney,
                    CO_DiscountMoney = e.CO_DiscountMoney,
                    CO_GavePoint = e.CO_GavePoint,
                    CO_Recash = e.CO_Recash,
                    CO_CreateTime = e.CO_CreateTime,
                }).ToList();
            var result = new { total = rowCount, rows = pageDate };
            return new OpearteResult(ResultStatus.Success, "", result);

        }
        #endregion

        #region 马上结算

        /// <summary>
        /// 马上结算
        /// </summary>
        /// <param name="memCards"></param>
        /// <returns></returns>
        public OpearteResult GotoBalance(GetPageMemCardsViewModel memCards, int id)
        {
          
            var result = Find(e => e.MC_CardID == memCards.MC_CardID);
            if (result != null)//根据卡号查出信息
            {
                ConsumeOrders consumeOrders = new ConsumeOrders()
                {
                    S_ID = result.Shops.S_ID,//店铺编号
                    U_ID = id,//用户编号
                    CO_OrderCode = RandomNumber.GetRandom(),//订单号
                    CO_OrderType = 5,//订单类型
                    MC_ID = result.MC_ID,//会员编号
                    MC_CardID = result.MC_CardID,//会员卡号
                    EG_ID = 0,//礼品编号
                    CO_TotalMoney = memCards.CO_TotalMoney,//CL_Percent额度
                    CO_DiscountMoney = memCards.CO_TotalMoney * memCards.CL_Percent,//实际支付
                    CO_GavePoint = 0,//兑/减积分
                    CO_Recash = 0,//积分返现
                    CO_Remark = "快速消费",//备注
                    CO_CreateTime = DateTime.Now//消费时间

                };

                if (Add(consumeOrders))
                {
                    //同时对表MemCards里面的数据进行相关的修改[修改积分]
                    var data = memCardsService.Find(e => e.MC_CardID == memCards.MC_CardID);
                    if (data != null)
                    {
                        MemCards menber = new MemCards();
                        //更新当前积分和累计消费
                        data.MC_Point += 10;
                        data.MC_TotalMoney += float.Parse(consumeOrders.CO_DiscountMoney.ToString());
                        //更改的方法
                        if (memCardsService.Update(data))
                        {
                            MemCardsViewModel viewModel = new MemCardsViewModel()
                            {
                                MC_Point = data.MC_Point,
                                MC_TotalMoney = data.MC_TotalMoney,
                                CO_DiscountMoney= memCards.CO_TotalMoney * memCards.CL_Percent,
                                CL_PointTo = Convert.ToInt32(memCards.CO_TotalMoney * memCards.CL_Point)
                            };
                            return new OpearteResult(ResultStatus.Success, "添加成功", viewModel);
                        }
                    }
                    return new OpearteResult(ResultStatus.Success, "添加成功");
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "添加失败");
                }  
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "数据不存在");
            }
        }

        #endregion

        #region 会员减积分（马上扣除）
        public OpearteResult DeductMemberPoint(GetPageMemCardsViewModel memConsume,int id)
        {
            ConsumeOrders consumeOrders = new ConsumeOrders()
            {
                U_ID = id,//  --用户编号
                S_ID = Find(e=>e.U_ID==id).Shops.S_ID,// --店铺编号
                CO_OrderCode = RandomNumber.GetRandom(),//订单号
                CO_OrderType = 3,// --订单类型
                MC_ID = 1,//--会员编号
                MC_CardID = memConsume.MC_CardID,// --会员卡号
                EG_ID = 0,//--礼品编号
                CO_TotalMoney = 0,//额度
                CO_DiscountMoney = 0,//  --实际支付
                CO_GavePoint = memConsume.CO_GavePoint,//  --兑 / 减积分
                CO_Recash = 0,//--积分返现   
                CO_Remark = memConsume.CO_Remark,//备注
                CO_CreateTime = DateTime.Now,
            };
            if (Add(consumeOrders))
            {
                //对表MemCards里面的数据进行相关的修改
                var data = memCardsService.Find(e => e.MC_CardID == memConsume.MC_CardID);
                if (data != null)
                {
                     data.MC_Point -= memConsume.CO_GavePoint;
                    //调用会员里面的更新方法，对会员积分进行更新
                    if (memCardsService.Update(data))
                    {
                        MemCardsViewModel memCards = new MemCardsViewModel()
                        {
                            MC_Point = data.MC_Point
                        };
                        return new OpearteResult(ResultStatus.Success, "修改成功", memCards);
                    }
                }
                return new OpearteResult(ResultStatus.Success, "添加成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "添加失败");
            }

        }


        #endregion

        #region 积分返现

        /// <summary>
        /// 积分返现（添加ConsumeOrders表的数据，修改MemCards的数据）
        /// </summary>
        /// <param name="getMember"></param>
        /// <returns></returns>
        public OpearteResult GoTORecash(GetPageMemCardsViewModel getMember)
        {
            //添加ConsumeOrders数据
            ConsumeOrders consumeOrders = new ConsumeOrders()
            {
                S_ID = 1,
                U_ID = 1,
                CO_OrderCode = RandomNumber.GetRandom(),
                CO_OrderType = 3,
                MC_ID = 1,
                MC_CardID = getMember.MC_CardID,
                EG_ID = 0,
                CO_TotalMoney = 0,
                CO_DiscountMoney = 0,
                CO_GavePoint = getMember.CO_GavePoint,
                CO_Recash =getMember.CO_GavePoint * getMember.CL_Percent,
                CO_Remark = "积分返现",
                CO_CreateTime = DateTime.Now,
            };
            if (Add(consumeOrders))
            {
                //对表MemCards里面的数据进行相关的修改
                var data = memCardsService.Find(e => e.MC_CardID == getMember.MC_CardID);
                if (data != null)
                {
                    data.MC_Point -= getMember.CO_GavePoint;
                    //调用会员里面的更新方法，对会员积分进行更新
                    if (memCardsService.Update(data))
                    {
                        MemCardsViewModel memCards = new MemCardsViewModel()
                        {
                            MC_Point = data.MC_Point,
                            CO_Recash = Convert.ToDecimal(getMember.CO_GavePoint * getMember.CL_Percent),
                            CO_CreateTime = DateTime.Now.ToString()
                        };
                        return new OpearteResult(ResultStatus.Success, "修改成功", memCards);
                    }
                }
                return new OpearteResult(ResultStatus.Success, "添加成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "添加失败");
            }

        }



        #endregion

        MemCardsService mem = new MemCardsService();
        public OpearteResult GetConsumeOrderList(GetPageConsumeOrdersViewModel getpage)
        {
            var list = PredicateExtensions.True<ConsumeOrders>();
            int pageCount = 0;
            if (!string.IsNullOrWhiteSpace(getpage.beginTime))
            {
                DateTime beginTime = Convert.ToDateTime(getpage.beginTime);
                list = list.And(e => e.CO_CreateTime >= beginTime);
            }
            if (!string.IsNullOrWhiteSpace(getpage.EndTime))
            {
                DateTime endTime = Convert.ToDateTime(getpage.EndTime);
                list = list.And(e => e.CO_CreateTime < endTime);
            }
            if (!string.IsNullOrWhiteSpace(getpage.CO_OrderCode))
            {

                list = list.And(e => e.CO_OrderCode.Contains(getpage.CO_OrderCode));
            }

            if (!string.IsNullOrWhiteSpace(getpage.fuhao))
            {
                var fuhao = getpage.fuhao;
                if (!string.IsNullOrWhiteSpace(getpage.money))
                {
                    float money = float.Parse(getpage.money);
                    if (fuhao == ">")
                    {
                        list = list.And(e => e.CO_TotalMoney > money);
                    }
                    else if (fuhao == "=")
                    {
                        list = list.And(e => e.CO_TotalMoney == money);
                    }
                    else
                    {
                        list = list.And(e => e.CO_TotalMoney < money);
                    }
                }
                else if (!string.IsNullOrWhiteSpace(getpage.GavePoint))
                {
                    decimal GavePoint = Convert.ToDecimal(getpage.GavePoint);
                    if (fuhao == ">")
                    {
                        list = list.And(e => e.CO_GavePoint > GavePoint);
                    }
                    else if (fuhao == "=")
                    {
                        list = list.And(e => e.CO_GavePoint == GavePoint);
                    }
                    else
                    {
                        list = list.And(e => e.CO_GavePoint < GavePoint);
                    }
                }
            }
            
            if (!string.IsNullOrWhiteSpace(getpage.CardId))
            {

                list = list.And(e => e.MC_CardID == getpage.CardId);
            }
            var query = GetListPage(getpage.page, getpage.rows, ref pageCount, list, e => e.CO_ID, true).Select(e => new ConsumeOrdersViewModel
            {
                CO_OrderCode = e.CO_OrderCode,
                MC_CardID = e.MC_CardID,
                MC_ID = e.MC_ID,
                //MC_Name = mem.Find(a => a.MC_ID==e.MC_ID).MC_Name,
                CO_GavePoint = e.CO_GavePoint,
                CO_Recash = e.CO_Recash,
                CO_CreateTime = e.CO_CreateTime,
                CO_TotalMoney = e.CO_TotalMoney

            }).ToList();
            foreach (var item in query)
            {
                item.MC_Name = mem.Find(a => a.MC_ID == item.MC_ID).MC_Name;
            }
            var result = new { total = pageCount, rows = query };
            return new OpearteResult(ResultStatus.Success, "", result);
        }

    }
}
