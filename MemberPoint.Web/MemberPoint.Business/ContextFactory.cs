﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MemberPoint.Entity.POCOModel;
using System.Data.Entity;
using System.Runtime.Remoting.Messaging;


namespace MemberPoint.Business
{

    /// <summary>
    /// 从线程中获取数据访问上下文，保证线程内实例唯一
    /// </summary>
    public class ContextFactory
    {
        public static DbContext GetCurrentContext()
        {
            DbContext context = CallContext.GetData("MemberPointDB") as DbContext;
            if (context == null)
            {
                context = new MemberPointDB();
                CallContext.SetData("MemberPointDB", context);
            }
            return context;
        }
        

    }
}
