﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Common;

namespace MemberPoint.Business
{
    /// <summary>
    ///  礼品兑换记录（ExchangLogs）
    /// </summary>
    public class ExchangLogsService :BaseService<ExchangLogs>
    {

        /// <summary>
        ///  礼品兑换记录（ExchangLogs）
        /// </summary>
            MemCardsService _MemCardsService = new MemCardsService();//会员
            ConsumeOrdersService _consumeOrders = new ConsumeOrdersService();//消费订单
            ExchangeGiftsService _ExchangGiftsService = new ExchangeGiftsService();//礼品
          

            #region 礼品兑换记录
            /// <summary>
            /// 礼品兑换记录
            /// </summary>
            /// <returns></returns>
            public OpearteResult GetExchangLogsInfo()
            {
                var query = GetList(e => true).Select(e => new ExchangLogsViewModel
                {
                    EL_ID = e.EL_ID,
                    S_ID = e.S_ID,
                    U_ID = e.U_ID,
                    MC_ID = e.MC_ID,
                    MC_CardID = e.MC_CardID,
                    MC_Name = e.MC_Name,
                    EG_ID = e.EG_ID,
                    EG_GiftCode = e.EG_GiftCode,
                    EG_GiftName = e.EG_GiftName,
                    EL_Number = e.EL_Number,
                    EL_Point = e.EL_Point,
                    EL_CreateTime = e.EL_CreateTime,
                }).OrderByDescending(e => e.EL_ID).ToList();
                return new OpearteResult(ResultStatus.Success, "", query);


            }


            /// <summary>
            /// 礼品兑换记录
            /// </summary>
            /// <param name="getExchangLogs"></param>
            /// <returns></returns>
            public OpearteResult GetExchangLogsInfo(GetPageExchangLogsViewModel getExchangLogs)
            {
                var query = PredicateExtensions.True<ExchangLogs>();
                if (!string.IsNullOrWhiteSpace(getExchangLogs.MC_CardID))
                {
                    query = query.And(e => e.MC_CardID.Contains(getExchangLogs.MC_CardID));
                }
                if (!string.IsNullOrWhiteSpace(getExchangLogs.startTime))
                {
                    DateTime StartTime = Convert.ToDateTime(getExchangLogs.startTime);
                    query = query.And(e => e.EL_CreateTime >= StartTime);
                }
                if (!string.IsNullOrWhiteSpace(getExchangLogs.endTime))
                {
                    DateTime EndTime = Convert.ToDateTime(getExchangLogs.endTime);
                    query = query.And(e => e.EL_CreateTime < EndTime);
                }
                int rowCount = 0;
                var pageDate = GetListPage(getExchangLogs.page, getExchangLogs.rows, ref rowCount, query, e => e.U_ID, true)
                    .Select(e => new ExchangLogsViewModel
                    {
                        EL_ID = e.EL_ID,
                        S_ID = e.S_ID,
                        U_ID = e.U_ID,
                        MC_ID = e.MC_ID,
                        MC_CardID = e.MC_CardID,
                        MC_Name = e.MC_Name,
                        EG_ID = e.EG_ID,
                        EG_GiftCode = e.EG_GiftCode,
                        EG_GiftName = e.EG_GiftName,
                        EL_Number = e.EL_Number,
                        EL_Point = e.EL_Point,
                        EL_CreateTime = e.EL_CreateTime,
                    }).ToList();
                var result = new { total = rowCount, rows = pageDate };
                return new OpearteResult(ResultStatus.Success, "", result);

            }

            #endregion

            #region 马上兑换

            /// <summary>
            /// 马上兑换
            /// </summary>
            /// <param name="shuju"></param>
            /// <returns></returns>
            public OpearteResult ComeConversion(string shuju,string MC_CardID,int userid)
            {
            string bianliang = shuju.Substring(0, shuju.Length - 1);
            string[] str = bianliang.Split(',');
            string EG_ID = "";
            // str.(num);//根据集合删除指定下标的元素
            if (str != null)
            {
                foreach (string item in str)
                {
                    EG_ID = item;
                    int egid = Convert.ToInt32(EG_ID);
                    //查询数据是否存在 ExchangGifts
                    var date = _ExchangGiftsService.Find(e => e.EG_ID == egid);
                    var _MemCards = _MemCardsService.Find(e=>e.MC_CardID==MC_CardID);
                    if (date != null)
                    {
                        // 把兑换基本信息保存到表ExchangLogs表中。
                        ExchangLogs el = new ExchangLogs()
                        {
                            S_ID = date.S_ID,//店铺编号
                            U_ID = userid,//用户编号
                            MC_ID = _MemCards.MC_ID,//会员编号
                            MC_CardID = _MemCards.MC_CardID,//会员卡号
                            MC_Name = _MemCards.MC_Name,//会员姓名
                            EG_ID = date.EG_ID,//礼品编号
                            EG_GiftCode = date.EG_GiftCode,//礼品编码
                            EG_GiftName = date.EG_GiftName,//礼品名称
                            EL_Number = 1,//兑换数量
                            EL_Point = date.EG_Point,//所用积分
                            EL_CreateTime = DateTime.Now,//兑换时间
                           
                        };
                        //添加成功
                        if (Add(el))
                        {
                            // 同时对表MemCards里面的数据进行相关的修改。
                            var _MemCardsto = _MemCardsService.Find(e=>e.MC_CardID==MC_CardID);
                            if (_MemCardsto != null)
                            {
                                //更新MC_Point当前积分
                                _MemCards.MC_Point -= el.EL_Point;
                                if (_MemCardsService.Update(_MemCardsto))
                                {
                                    int id = (int)el.EG_ID;
                                    //同时对表ExchangGifts里面的数据进行相关的修改
                                    var _exchangGifts = _ExchangGiftsService.Find(e=>e.EG_ID==id);
                                    if (_exchangGifts != null)
                                    {
                                        _exchangGifts.EG_Number -= 1;//总数量
                                        _exchangGifts.EG_ExchangNum += 1;//已兑换的数量
                                        if (_ExchangGiftsService.Update(_exchangGifts))
                                        {
                                            // return new ResultState(EnumHelper.Success, "更新成功！");
                                        }
                                        else
                                        {
                                            //return new ResultState(EnumHelper.Error, "更新失败！");
                                        }

                                    }
                                    else
                                    {
                                        //return new ResultState(EnumHelper.Error, "礼品信息数据不存在哦！");
                                    }

                                }
                                else
                                {
                                    // return new ResultState(EnumHelper.Error, "更新失败！");
                                }
                            }
                            else
                            {
                                // return new ResultState(EnumHelper.Error, "会员信息数据不存在哦！");
                            }

                        }
                        else
                        {
                            // return new ResultState(EnumHelper.Error, "添加失败！");
                        }
                    }
                    else
                    {
                        //  return new ResultState(EnumHelper.Error, "数据不存在哦！");
                    }
                }
                return new OpearteResult(ResultStatus.Success, "添加成功过！");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "数据不存在哦！");
            }
            //return new ResultState();
        }

        #endregion


    }
}
