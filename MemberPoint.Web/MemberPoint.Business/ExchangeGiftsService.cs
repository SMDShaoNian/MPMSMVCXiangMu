﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Common;



namespace MemberPoint.Business
{
  public  class ExchangeGiftsService:BaseService<ExchangGifts>
    {

        #region 积分兑换礼品
        public OpearteResult PointChangeGifts(GetPageMemCardsViewModel getMemCards)
        {
            var query = PredicateExtensions.True<ExchangGifts>();
            int rowCount = 0;
            var pageDate = GetListPage(getMemCards.page, getMemCards.rows, ref rowCount, query, e => e.EG_ID, true)
                .Select(e => new
                {
                    EG_ID = e.EG_ID,
                    S_ID = e.S_ID,
                    EG_GiftCode = e.EG_GiftCode,
                    EG_GiftName = e.EG_GiftName,
                    EG_Photo = e.EG_Photo,
                    EG_Point = e.EG_Point,
                    EG_Number = e.EG_Number,
                    EG_ExchangNum = e.EG_ExchangNum,
                    EG_Remark = e.EG_Remark
                }).ToList();
            var result = new { total = rowCount, rows = pageDate };
            return new OpearteResult(ResultStatus.Success, "", result);

        }
        #endregion

        #region 修改已兑换数量
        public OpearteResult GetExchangGiftsEG_ID(int id)
        {
            var data = Find(e => e.EG_ID == id);
            if (data != null)
            {
                ExchangGiftsViewModel gift = new ExchangGiftsViewModel
                {
                    EG_ID = data.EG_ID,
                    S_ID = data.S_ID,
                    EG_GiftCode = data.EG_GiftCode,
                    EG_GiftName = data.EG_GiftName,
                    EG_Photo = data.EG_Photo,
                    EG_Point = data.EG_Point,
                    EG_Number = data.EG_Number,
                    EG_ExchangNum = data.EG_ExchangNum,
                    EG_Remark = data.EG_Remark,
                };
                return new OpearteResult(ResultStatus.Success, "", gift);
            }
            else
            {
                return new OpearteResult(ResultStatus.Success, "此礼品不存在");
            }
        }
        /// <summary>
        /// 修改已兑换数量
        /// </summary>
        /// <returns></returns>
        public OpearteResult EditEG_ExchangNum(ExchangGiftsViewModel gift)
        {
            //获取实体
            var gifts = Find(e => e.EG_ID == gift.EG_ID);
            if (gifts != null)
            {
                gifts.EG_ExchangNum = gift.EG_ExchangNum;

                if (Update(gifts))
                {
                    return new OpearteResult(ResultStatus.Success, "编辑成功");
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "编辑失败");
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "该信息不存在");
            }
        }

        #endregion




        /// <summary>
        /// 查询礼品列表
        /// </summary>
        /// <returns></returns>
        public OpearteResult GetExchang()
        {
            OpearteResult result;
            var ExchangList = GetList(e => true).Select(e => new ExchangGiftsViewModel
            {
                EG_ID = e.EG_ID,
                S_ID = e.S_ID,
                EG_GiftCode = e.EG_GiftCode,
                EG_ExchangNum = e.EG_ExchangNum,
                EG_GiftName = e.EG_GiftName,
                EG_Number = e.EG_Number,
                EG_Photo = e.EG_Photo,
                EG_Point = e.EG_Point,
                EG_Remark = e.EG_Remark

            }).ToList();
            return result = new OpearteResult(ResultStatus.Success, "", ExchangList);
        }
        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="exchang"></param>
        /// <returns></returns>
        public OpearteResult AddExchang(ExchangGiftsViewModel exchang)
        {
            OpearteResult result;
            ExchangGifts exchan = new ExchangGifts
            {
                EG_ID = exchang.EG_ID,
                S_ID = exchang.S_ID,
                EG_GiftCode = exchang.EG_GiftCode,
                EG_ExchangNum = exchang.EG_ExchangNum,
                EG_GiftName = exchang.EG_GiftName,
                EG_Number = exchang.EG_Number,
                EG_Photo = exchang.EG_Photo,
                EG_Point = exchang.EG_Point,
                EG_Remark = exchang.EG_Remark
            };
            if (Add(exchan))
            {
                result = new OpearteResult(ResultStatus.Success, "添加成功");
            }
            else
            {
                result = new OpearteResult(ResultStatus.Error, "添加失败");
            }
            return result;
        }

        /// <summary>
        /// 查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExchangGiftsViewModel showExchang(int id)
        {

            var query = Find(e => e.EG_ID == id);
            var list = new ExchangGiftsViewModel
            {


                EG_ID = query.EG_ID,
                S_ID = query.S_ID,
                EG_GiftCode = query.EG_GiftCode,
                EG_ExchangNum = query.EG_ExchangNum,
                EG_GiftName = query.EG_GiftName,
                EG_Number = query.EG_Number,
                EG_Photo = query.EG_Photo,
                EG_Point = query.EG_Point,
                EG_Remark = query.EG_Remark
            };
            return list;
        }
        /// <summary>
        /// 修改礼品
        /// </summary>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        public OpearteResult UpdateExchang(ExchangGiftsViewModel exchang, int id)
        {
            var query = Find(e => e.EG_ID == id);
            if (query != null)
            {
                query.EG_ID = exchang.EG_ID;
                query.S_ID = exchang.S_ID;
                query.EG_GiftCode = exchang.EG_GiftCode;
                query.EG_ExchangNum = exchang.EG_ExchangNum;
                query.EG_GiftName = exchang.EG_GiftName;
                query.EG_Number = exchang.EG_Number;
                query.EG_Photo = exchang.EG_Photo;
                query.EG_Point = exchang.EG_Point;
                query.EG_Remark = exchang.EG_Remark;
                if (Update(query))
                {
                    return new OpearteResult(ResultStatus.Success, "修改成功");
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "修改失败");
                }

            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "找不到该条信息");
            }

        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="userinfo"></param>
        /// <returns></returns>
        public OpearteResult DeleteExchang(int id)
        {
            var query = Find(e => e.EG_ID == id);
            if (query != null)
            {
                if (Delete(query))
                {
                    return new OpearteResult(ResultStatus.Success, "删除成功", query);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "删除失败", query);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "找不到该条信息", query);
            }


        }
        /// <summary>
        ///根据ID查询礼品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult GetExchangFirst(int id)
        {
            OpearteResult result;
            var exchang = Find(i => i.EG_ID == id);
            var exchangList = new ExchangGiftsViewModel
            {
                EG_ID = exchang.EG_ID,
                S_ID = exchang.S_ID,
                EG_GiftCode = exchang.EG_GiftCode,
                EG_ExchangNum = exchang.EG_ExchangNum,
                EG_GiftName = exchang.EG_GiftName,
                EG_Number = exchang.EG_Number,
                EG_Photo = exchang.EG_Photo,
                EG_Point = exchang.EG_Point,
                EG_Remark = exchang.EG_Remark
            };
            return result = new OpearteResult(ResultStatus.Success, "", exchangList);
        }
        /// <summary>
        /// 排序
        /// </summary>
        /// <param name="pagefen"></param>
        /// <returns></returns>
        public OpearteResult GetExchangGifts(PageFenYe pagefen)
        {
            var query = PredicateExtensions.True<ExchangGifts>();

            int rowCount = 0;
            var pageDate = GetList(pagefen.page, pagefen.rows, ref rowCount, query, e => e.EG_ID, true)
                .Select(e => new ExchangGiftsViewModel
                {
                    EG_ID = e.EG_ID,
                    S_ID = e.S_ID,
                    EG_GiftName = e.EG_GiftName,
                    EG_ExchangNum = e.EG_ExchangNum,
                    EG_GiftCode = e.EG_GiftCode,
                    EG_Number = e.EG_Number,
                    EG_Photo = e.EG_Photo,
                    EG_Point = e.EG_Point,
                    EG_Remark = e.EG_Remark

                }).ToList();
            var result = new { total = rowCount, rows = pageDate };
            return new OpearteResult(ResultStatus.Success, "", result);

        }


    }
}
