﻿using System;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using System.Data.SqlClient;
using System.Data.Entity;
using MemberPoint.Entity.DTOModel;
using System.Security.Principal;
using System.Data.Entity.Core.Metadata.Edm;

namespace MemberPoint.Business
{
  public  class MemCardsService:BaseService<MemCards>
    {
        #region 查找会员
        /// <summary>
        /// 查找会员
        /// </summary>
        /// <param name="getUser"></param>
        /// <returns></returns>
        public OpearteResult  FindMemCards(GetPageMemCardsViewModel getMemCards)
        {
            var data = Find(e => e.MC_CardID == getMemCards.MC_CardID);
            if (data != null)
            {

                var viewModel = new MemCardsViewModel
                {
                    MC_Name = data.MC_Name,
                    MC_Point = data.MC_Point,
                    MC_TotalMoney = data.MC_TotalMoney,
                    CL_LevelName = data.CardLevels.CL_LevelName,
                    MC_CreateTimeTo = data.MC_CreateTime.ToString(),
                    CL_Percent = data.CardLevels.CL_Percent,
                    CL_Point = data.CardLevels.CL_Point
                };
                return new OpearteResult(ResultStatus.Success, "查询成功", viewModel);
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "查询成功", data);
            }


          
        }
        #endregion


        /// <summary>
        /// 分页获取会员列表信息
        /// </summary>
        /// <param name="model">分页所要的参数</param>
        /// <param name="S_ID">当前用户ID</param>
        /// <returns></returns>
        public OpearteResult GetMemCardsList(GetPageViewModel model, int? S_ID)
        {
            var query = PredicateExtensions.True<MemCards>();
            query = query.And(m => m.S_ID == S_ID);
            if (!string.IsNullOrWhiteSpace(model.MC_Name))
            {
                query = query.And(m => m.MC_Name.Contains(model.MC_Name));
            }
            if (!string.IsNullOrWhiteSpace(model.MC_CardID))
            {
                query = query.And(m => m.MC_CardID.Contains(model.MC_CardID));
            }
            if (!string.IsNullOrWhiteSpace(model.MC_Mobile))
            {
                query = query.And(m => m.MC_Mobile.Contains(model.MC_Mobile));
            }
            if (model.CL_ID.HasValue && model.CL_ID != 99)
            {
                query = query.And(m => m.CL_ID == model.CL_ID);
            }
            if (model.MC_State.HasValue && model.MC_State != 99)
            {
                query = query.And(m => m.MC_State == model.MC_State);
            }
            int totalCount = 0;
            var MemCardsInfo = GetListPage(model.page, model.rows, ref totalCount, query, u => u.MC_CardID, true).Select(u => new MemCardsViewModel
            {
                //MC_ID=u.MC_ID,
                MC_CardID = u.MC_CardID,
                MC_Name = u.MC_Name,
                MC_Mobile = u.MC_Mobile,
                MC_TotalMoney = u.MC_TotalMoney,
                MC_State = u.MC_State,
                MC_Point = u.MC_Point,
                MC_Sex = u.MC_Sex,
                MC_CreateTime = u.MC_CreateTime,
                EnumSex = ((EnumTypes.Sex)u.MC_Sex).ToString(),
                EnumState = ((EnumTypes.State)u.MC_State).ToString(),
                LeveType = ((EnumTypes.LeveType)u.CL_ID).ToString()
            }).ToList();
            var list = new { total = totalCount, rows = MemCardsInfo };
            return new OpearteResult(ResultStatus.Success, "", list);
        }
        /// <summary>
        /// 会员挂失查询
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public OpearteResult GetBycard(string card)
        {

            var list = Find(m => m.MC_CardID == card);
            MemCardsViewModel mem = new MemCardsViewModel()
            {
                MC_CardID = list.MC_CardID,
                MC_State = list.MC_State
            };
            return new OpearteResult(ResultStatus.Success, "", mem);
        }

        /// <summary>
        /// 添加会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OpearteResult AddInfo(MemCardsViewModel model)
        {
            OpearteResult result;
            if (Find(m => m.MC_Name == model.MC_Name) == null)
            {
                var info = Find(m => m.MC_Name == model.MC_RefererName);
                if (info != null)
                {

                    MemCards memcards = new MemCards
                    {
                        CL_ID = model.CL_ID,
                        MC_BirthdayType = Convert.ToByte(model.MC_BirthdayType == true ? 0 : 1),
                        MC_Birthday_Day = model.MC_Birthday_Day,
                        MC_Birthday_Month = model.MC_Birthday_Month,
                        MC_CardID = RandomNumber.GetRandom(),
                        MC_CreateTime = DateTime.Now,
                        MC_IsPast = model.MC_IsPast == true ? true : false,
                        MC_IsPointAuto = model.MC_IsPointAuto == true ? true : false,
                        MC_Mobile = model.MC_Mobile,
                        MC_Money = model.MC_Money,
                        MC_Name = model.MC_Name,
                        MC_Password = model.MC_Password,
                        MC_PastTime = model.MC_PastTime,
                        MC_Photo = "无",
                        MC_Point = model.MC_Point,
                        MC_RefererCard = info.MC_CardID,
                        MC_RefererID = info.MC_ID,
                        MC_RefererName = model.MC_RefererName,
                        MC_Sex = model.MC_Sex,
                        MC_State = model.MC_State,
                        MC_TotalCount = 0,
                        MC_TotalMoney = 0,
                        S_ID = 2,
                        MC_OverCount = 0
                    };
                    if (Add(memcards))
                    {
                        return result = new OpearteResult(ResultStatus.Success, "添加成功");
                    }
                    else
                    {
                        return result = new OpearteResult(ResultStatus.Error, "添加失败");
                    }
                }
                else
                {
                    return result = new OpearteResult(ResultStatus.Error, "推荐人不存在");
                }

            }
            else
            {
                return result = new OpearteResult(ResultStatus.Error, "姓名已存在");
            }
        }
        /// <summary>
        /// 根据卡号获取数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult Getfirst(string id)
        {
            var list = Find(m => m.MC_CardID == id);
            if (list != null)
            {
                MemCardsViewModel meminfo = new MemCardsViewModel
                {
                    MC_ID = list.MC_ID,
                    MC_CardID = list.MC_CardID,
                    MC_Name = list.MC_Name,
                    CL_ID = list.CL_ID,
                    MC_BirthdayType = list.MC_BirthdayType == 0 ? true : false,
                    MC_Birthday_Day = list.MC_Birthday_Day,
                    MC_Birthday_Month = list.MC_Birthday_Month,
                    MC_IsPast = list.MC_IsPast == true ? true : false,
                    MC_IsPointAuto = list.MC_IsPointAuto == true ? true : false,
                    MC_Mobile = list.MC_Mobile,
                    MC_PastTime = list.MC_PastTime,
                    MC_Point = list.MC_Point,
                    MC_RefererName = list.MC_RefererName,
                    MC_Sex = list.MC_Sex,
                    MC_State = list.MC_State,
                    MC_Money = list.MC_Money,
                    MC_TotalMoney = list.MC_TotalMoney
                };
                return new OpearteResult(ResultStatus.Success, "", meminfo);
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "为空");
            }

        }
        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OpearteResult UpdateMemCards(MemCardsViewModel model)
        {
            OpearteResult result;
            var list = Find(m => m.MC_ID == model.MC_ID);
            if (list != null)
            {
                var info = Find(m => m.MC_Name == model.MC_RefererName);
                if (info != null)
                {
                    //MemCards memcards = new MemCards
                    //{
                    //    MC_ID=model.MC_ID,
                    //    CL_ID = model.CL_ID,
                    //     MC_CardID=model.MC_CardID,
                    //      MC_Photo="无",
                    //    MC_BirthdayType = Convert.ToByte(model.MC_BirthdayType == true ? 0 : 1),
                    //    MC_Birthday_Day = model.MC_Birthday_Day,
                    //    MC_Birthday_Month = model.MC_Birthday_Month,
                    //    MC_IsPast = Convert.ToByte(model.MC_IsPast == true ? 0 : 1),
                    //    MC_IsPointAuto = Convert.ToByte(model.MC_IsPointAuto == true ? 0 : 1),
                    //    MC_Mobile = model.MC_Mobile,
                    //    MC_Money = model.MC_Money,
                    //    MC_Name = model.MC_Name,
                    //    MC_Password = model.MC_Password,
                    //    MC_PastTime = model.MC_PastTime,
                    //    MC_Point = model.MC_Point,
                    //    MC_RefererCard = info.MC_CardID,
                    //    MC_RefererID = info.MC_ID,
                    //    MC_RefererName = model.MC_RefererName,
                    //    MC_Sex = model.MC_Sex,
                    //    MC_State = model.MC_State,
                    //     MC_CreateTime=DateTime.Now,
                    //       S_ID=2
                    //};
                    list.MC_ID = model.MC_ID;
                    list.CL_ID = model.CL_ID;
                    list.MC_BirthdayType = Convert.ToByte(model.MC_BirthdayType == true ? 0 : 1);
                    list.MC_Birthday_Day = model.MC_Birthday_Day;
                    list.MC_Birthday_Month = model.MC_Birthday_Month;
                    list.MC_IsPast = model.MC_IsPast == true ? true : false;
                    list.MC_IsPointAuto = model.MC_IsPointAuto == true ? true : false;
                    list.MC_Mobile = model.MC_Mobile;
                    list.MC_Money = model.MC_Money;
                    list.MC_Name = model.MC_Name;
                    list.MC_Password = model.MC_Password;
                    list.MC_PastTime = model.MC_PastTime;
                    list.MC_Point = model.MC_Point;
                    list.MC_RefererCard = info.MC_CardID;
                    list.MC_RefererName = model.MC_RefererName;
                    list.MC_Sex = model.MC_Sex;
                    list.MC_State = model.MC_State;
                    if (Update(list))
                    {
                        return result = new OpearteResult(ResultStatus.Success, "修改成功");
                    }
                    else
                    {
                        return result = new OpearteResult(ResultStatus.Error, "修改失败");
                    }
                }
                else
                {
                    return result = new OpearteResult(ResultStatus.Error, "推荐人不存在");
                }

            }
            else
            {
                return result = new OpearteResult(ResultStatus.Error, "用户不已存在");
            }

        }

        /// <summary>
        /// 删除会员信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult DeleteMemCards(string id)
        {
            var list = Find(m => m.MC_CardID == id);
            if (list != null)
            {
                if (Delete(list))
                {
                    return new OpearteResult(ResultStatus.Success, "删除成功");
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "删除失败");
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "该会员不存在");
            }

        }

        /// <summary>
        /// 挂失/锁定
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OpearteResult LockMemCards(MemCardsViewModel model)
        {
            var list = Find(m => m.MC_CardID == model.MC_CardID);
            if (list != null)
            {
                //MemCards mem = new MemCards
                //{
                //    MC_CardID = model.MC_CardID,
                //    MC_State = model.MC_State
                //};
                list.MC_State = model.MC_State;
                if (Update(list))
                {
                    return new OpearteResult(ResultStatus.Success, "挂失/锁定成功");
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "挂失/锁定失败");
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "会员不存在");
            }
        }

        /// <summary>
        /// 换卡查询
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public OpearteResult ResetGetBycard(string card)
        {

            var list = Find(m => m.MC_CardID == card);
            MemCardsViewModel mem = new MemCardsViewModel()
            {
                MC_CardID = list.MC_CardID,
                MC_Name = list.MC_Name,
                LeveType = ((EnumTypes.LeveType)list.CL_ID).ToString(),
                MC_CreateTime = list.MC_CreateTime
            };
            return new OpearteResult(ResultStatus.Success, "", mem);
        }
        /// <summary>
        /// 换卡信息
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public OpearteResult ResetCard(MemCardsViewModel model)
        {

            var list = Find(m => m.MC_ID == model.MC_ID);
            MemCardsViewModel mem = new MemCardsViewModel()
            {
                MC_Name = list.MC_Name,
                LeveType = ((EnumTypes.LeveType)list.CL_ID).ToString(),
                MC_CreateTime = list.MC_CreateTime
            };
            return new OpearteResult(ResultStatus.Success, "", mem);
        }
        /// <summary>
        /// 更新换卡
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OpearteResult UpdateCards(MemCardsViewModel model)
        {
            OpearteResult result;
            var list = Find(m => m.MC_CardID == model.MC_CardID && m.MC_Password == model.OldPassword);
            if (list != null)
            {
                list.MC_CardID = RandomNumber.GetRandom();
                list.MC_Password = model.MC_Password;
                list.MC_RefererCard = model.MC_CardID;
                if (Update(list))
                {
                    return result = new OpearteResult(ResultStatus.Success, "换卡成功");
                }
                else
                {
                    return result = new OpearteResult(ResultStatus.Error, "换卡失败");
                }


            }
            else
            {
                return result = new OpearteResult(ResultStatus.Error, "卡原密码不正确");
            }

        }

        /// <summary>
        /// 转账
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OpearteResult VipTransfer(TransferViewModel model)
        {


            SqlParameter[] param ={
                new SqlParameter("@userid",model.UserId),
                 new SqlParameter("@formcard",model.Out_MC_CardID),
                     new SqlParameter("@tocard",model.In_MC_CardID),
                         new SqlParameter("@Money",model.TransferMoney),
                         new SqlParameter("@Remark",model.Remark),
                         new SqlParameter("@outMsg",System.Data.SqlDbType.NVarChar,200),
            };
            param[5].Direction = System.Data.ParameterDirection.Output;
            db.Database.ExecuteSqlCommand(@"EXEC[dbo].[P_VIPTransfer] @userid,@formcard,@tocard,@Money,@Remark ,@outMsg OUTPUT", param);
            if (string.IsNullOrWhiteSpace(param[5].Value.ToString()))
            {
                return new OpearteResult(ResultStatus.Success, "转账成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, param[5].Value.ToString());
            }



        }


    }
}
