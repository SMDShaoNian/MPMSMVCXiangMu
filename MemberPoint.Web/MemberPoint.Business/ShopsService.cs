﻿using MemberPoint.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Common;
using System.Collections;



namespace MemberPoint.Business
{
   public class ShopsService : BaseService<Shops>
    {
        /// <summary>
        /// 显示信息
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sid"></param>
        /// <param name="uname"></param>
        /// <returns></returns>
        public OpearteResult ShopInfo()
        {
            var list = GetList(s => true).Select(s => new ShopsViewModel
            {
                S_ID = s.S_ID,
                S_Name = s.S_Name,
                S_Category = s.S_Category,
                S_ContactName = s.S_ContactName,
                S_ContactTel = s.S_ContactTel,
                S_Address = s.S_Address,
                S_Remark = s.S_Remark,
                S_IsHasSetAdmin = s.S_IsHasSetAdmin,
                S_CreateTime = s.S_CreateTime
            }).ToList();
            return new OpearteResult(ResultStatus.Success, "成功", list);
        }
        public OpearteResult GetInfo(int page, int rows, string Sname, string SContactName, string adress)
        {
            var query = PredicateExtensions.True<Shops>();
            if (!string.IsNullOrWhiteSpace(Sname))
            {

                query = query.And(e => e.S_Name == Sname);
            }
            if (!string.IsNullOrWhiteSpace(SContactName))
            {
                query = query.And(e => e.S_ContactName.Contains(SContactName));
            }
            if (!string.IsNullOrWhiteSpace(adress))
            {
                query = query.And(e => e.S_Address.Contains(adress));
            }
            int rowCount = 0;
            var pageDate = GetListPage(page, rows, ref rowCount, query, e => e.S_ID, true).Select(a => new
            {
                S_ID = a.S_ID,
                S_Address = a.S_Address,
                S_IsHasSetAdmin = a.S_IsHasSetAdmin == true ? "已分配" : "未分配",
                S_Category = a.S_Category,
                S_ContactName = a.S_ContactName,
                S_ContactTel = a.S_ContactTel,
                S_CreateTime = a.S_CreateTime,
                S_Name = a.S_Name,
                S_Remark = a.S_Remark
            }).ToList();
            var dataGridViewModel = new { total = rowCount, rows = pageDate };
            return new OpearteResult(ResultStatus.Success, "", dataGridViewModel);
        }


        /// <summary>
        /// 添加店铺信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public OpearteResult AddShops(ShopsViewModel uvm)
        {
            var shops = GetList(e => true).FirstOrDefault(model => model.S_Name == uvm.S_Name);
            if (shops == null)
            {
                Shops ss = new Shops();
                ss.S_Name = uvm.S_Name;
                ss.S_Remark = uvm.S_Remark;
                ss.S_Address = uvm.S_Address;
                ss.S_Category = uvm.S_Category;
                ss.S_ContactName = uvm.S_ContactName;
                ss.S_ContactTel = uvm.S_ContactTel;
                ss.S_CreateTime = DateTime.Now;
                ss.S_IsHasSetAdmin = false;

                if (Add(ss))
                {
                    return new OpearteResult(ResultStatus.Success, "添加成功", uvm);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "添加失败", uvm);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "用户名已存在", uvm);
            }
        }

        public OpearteResult ShopsList()
        {
            var list = GetList(e => true).Select(c => new
            {
                S_ID = c.S_ID,
                S_Name = c.S_Name
            });
            return new OpearteResult(ResultStatus.Success, "", list);
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param nam e="id"></param>
        /// <returns></returns>
        public OpearteResult DeleteInfo(int id)
        {
            var shops = GetList(e => true).FirstOrDefault(model => model.S_ID == id);
            UserService us = new UserService();
            us.DeleteShopUser(id);
            if (Delete(shops))
            {

                return new OpearteResult(ResultStatus.Success, "删除成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "删除失败");
            }
        }
        /// <summary>
        /// 根据id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult GetShopsById(int id)
        {
            var b = Find(s => s.S_ID == id);
            ShopsViewModel a = new ShopsViewModel();
            a.S_ID = b.S_ID;
            a.S_ContactTel = b.S_ContactTel;
            a.S_Address = b.S_Address;
            a.S_Category = b.S_Category;
            a.S_ContactName = b.S_ContactName;
            a.S_ContactTel = b.S_ContactTel;
            a.S_Name = b.S_Name;
            a.S_Remark = b.S_Remark;
            a.S_IsHasSetAdmin = b.S_IsHasSetAdmin;
            a.S_CreateTime = b.S_CreateTime;
            return new OpearteResult(ResultStatus.Success, "成功", a);
        }
        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public OpearteResult EditShop(ShopsViewModel uvm, int id)
        {
            Shops s = new Shops();
            s.S_ID = id;
            s.S_Address = uvm.S_Address;
            s.S_Category = uvm.S_Category;
            s.S_ContactName = uvm.S_ContactName;
            s.S_ContactTel = uvm.S_ContactTel;
            s.S_CreateTime = DateTime.Now;
            s.S_Name = uvm.S_Name;
            s.S_Remark = uvm.S_Remark;
            s.S_IsHasSetAdmin = uvm.S_IsHasSetAdmin;
            bool a = Update(s);
            if (a)
            {
                return new OpearteResult(ResultStatus.Success, "修改成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "修改失败");
            }


        }
        /// <summary>
        /// 分配店长账号成功后更改店铺信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult UpdateShopsBoss(int id)
        {
            var b = Find(a => a.S_ID == id);
            b.S_ID = b.S_ID;
            b.S_Address = b.S_Address;
            b.S_Category = b.S_Category;
            b.S_ContactName = b.S_ContactName;
            b.S_ContactTel = b.S_ContactTel;
            b.S_CreateTime = DateTime.Now;
            b.S_Name = b.S_Name;
            b.S_Remark = b.S_Remark;
            b.S_IsHasSetAdmin = true;
            bool aa = Update(b);
            if (aa)
            {
                return new OpearteResult(ResultStatus.Success, "修改成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "修改失败");
            }

        }
        /// <summary>
        /// 根据id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ShopsViewModel GetOneShopsById(int id)
        {
            var b = Find(s => s.S_ID == id);
            ShopsViewModel a = new ShopsViewModel();
            a.S_ID = b.S_ID;
            a.S_ContactTel = b.S_ContactTel;
            a.S_Address = b.S_Address;
            a.S_Category = b.S_Category;
            a.S_ContactName = b.S_ContactName;
            a.S_ContactTel = b.S_ContactTel;
            a.S_Name = b.S_Name;
            a.S_Remark = b.S_Remark;
            a.S_IsHasSetAdmin = b.S_IsHasSetAdmin;
            a.S_CreateTime = b.S_CreateTime;
            return a;
        }



    }
}
