﻿using MemberPoint.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Common;
using static MemberPoint.Common.EnumTypes;
using MemberPoint.Entity.DTOModel;
using System.Web;
using System.Web.Security;

namespace MemberPoint.Business
{
   public  class UserService:BaseService<Users>
    {

        /// <summary>
        /// 获取用户的所有信息
        /// </summary>
        /// <returns></returns>
        public OpearteResult GetInfo(int page, int rows,string sid,string uname)
        {
            var query = PredicateExtensions.True<Users>();
            if (!string.IsNullOrWhiteSpace(sid))
            {
                int Sid = Convert.ToInt32(sid);
                query= query.And(e => e.S_ID == Sid);
            }
            if (!string.IsNullOrWhiteSpace(uname))
            {
                query= query.And(e => e.U_RealName.Contains(uname));
            }
            
            int rowCount = 0;
            var pageDate = GetListPage(page, rows, ref rowCount, query, e => e.U_ID, true).Select(a => new UsersViewModel
            {
                U_ID = a.U_ID,
                S_Name = a.Shops.S_Name,
                U_LoginName = a.U_LoginName,
                U_RealName = a.U_RealName,
                U_Sex = a.U_Sex,
                U_Telephone = a.U_Telephone,
                U_Role = (int)a.U_Role,
                U_RoleName = ((RoleTypeEnum)a.U_Role).ToString(),
                U_Password = a.U_Password,
                U_CanDelete = (bool)a.U_CanDelete
            }).ToList();
            var dataGridViewModel = new { total = rowCount, rows = pageDate };
            return new OpearteResult(ResultStatus.Success, "", dataGridViewModel);
        }


        /// <summary>
        /// 添加用户信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public OpearteResult AddUser(UsersViewModel uvm)
        {
            var user = GetList(e => true).FirstOrDefault(model => model.U_RealName == uvm.U_RealName);
            if (user == null)
            {
                Users us = new Users();
                us.S_ID = uvm.S_ID;
                us.U_LoginName = uvm.U_LoginName;
                us.U_RealName = uvm.U_RealName;
                us.U_Sex = uvm.U_Sex;
                us.U_Telephone = uvm.U_Telephone;
                us.U_Role = uvm.U_Role;
                us.U_Password = uvm.U_Password;
                us.U_CanDelete = uvm.U_CanDelete;
                if (Add(us))
                {
                    return new OpearteResult(ResultStatus.Success, "添加成功", uvm);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "添加失败", uvm);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "用户名已存在", uvm);
            }
        }
        /// <summary>
        /// 根据id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult GetUsersById(int id)
        {
            var b = Find(a => a.U_ID == id);
            UsersViewModel info = new UsersViewModel();
            info.U_ID = b.U_ID;
            info.S_ID = (int)b.S_ID;
            info.U_LoginName = b.U_LoginName;
            info.U_Password = b.U_Password;
            info.U_RealName = b.U_RealName;
            info.U_Sex = b.U_Sex;
            info.U_Telephone = b.U_Telephone;
            info.U_Role = (int)b.U_Role;
            info.U_CanDelete = (bool)b.U_CanDelete;
            return new OpearteResult(ResultStatus.Success, "成功", info);
        }



        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public OpearteResult EditUser(UsersViewModel uvm,int id)
        {
            Users users = new Users();
            users.U_ID = id;
            users.S_ID = uvm.S_ID;
            users.U_LoginName = uvm.U_LoginName;
            users.U_RealName = uvm.U_RealName;
            users.U_Sex = uvm.U_Sex;
            users.U_Telephone = uvm.U_Telephone;
            users.U_Role = uvm.U_Role;
            users.U_Password = uvm.U_Password;
            users.U_CanDelete = uvm.U_CanDelete;
            bool a = Update(users);
            if (a)
            {
                return new OpearteResult(ResultStatus.Success, "修改成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "修改失败");
            }
            

        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult DeleteInfo(int id)
        {
            var user = GetList(e => true).FirstOrDefault(model => model.U_ID == id);
            if (Delete(user))
            {
                return new OpearteResult(ResultStatus.Success, "删除成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "删除失败");
            }
        }
        

        /// <summary>
        /// 根据店铺删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OpearteResult DeleteShopUser(int id)
        {
            var user = GetList(e => true).FirstOrDefault(model => model.S_ID == id);
            if (Delete(user))
            {
                return new OpearteResult(ResultStatus.Success, "删除成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "删除失败");
            }
        }

        /// <summary>
        /// 分配店长账号(只填用户名，密码默认为123456，参数为选中的店铺，其余字段给默认值)
        /// </summary>
        /// <param name="uvm"></param>
        /// <returns></returns>
        public OpearteResult AddShopManager(UsersViewModel uvm)
        {

            var user = GetList(e => true).FirstOrDefault(model => model.U_LoginName == uvm.U_LoginName);
            if (user == null)
            {
                ShopsService sh = new ShopsService();
                ShopsViewModel a = sh.GetOneShopsById(uvm.S_ID);
                Users us = new Users();
                us.S_ID = uvm.S_ID;
                us.U_LoginName = uvm.U_LoginName;
                us.U_RealName = a.S_ContactName;
                us.U_Sex = "店长账号，待填";
                us.U_Telephone = a.S_ContactTel;
                us.U_Role = 2;
                us.U_Password = "123456";
                us.U_CanDelete = false;
                //bool a = true;
                if (Add(us))
                {

                    sh.UpdateShopsBoss(uvm.S_ID);
                    return new OpearteResult(ResultStatus.Success, "添加成功", uvm);
                }
                else
                {
                    return new OpearteResult(ResultStatus.Error, "添加失败", uvm);
                }
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "该商铺已经分配店长账号", uvm);
            }
        }




        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="uvm"></param>
        /// <returns></returns>
        public OpearteResult Login(UsersViewModel uvm)
        {
            var user = GetList(e => true).FirstOrDefault(model => model.U_LoginName == uvm.U_LoginName && model.U_Password == uvm.U_Password);
            if (user != null)
            {
                loginUserDataDtoModel userdata = new loginUserDataDtoModel
                {
                    U_ID = user.U_ID,
                    U_LoginName = user.U_LoginName,
                    U_Password = user.U_Password
                };
                SetAuthCookie(userdata);
                return new OpearteResult(ResultStatus.Success, "登录成功");
            }
            else
            {
                return new OpearteResult(ResultStatus.Error, "登录失败");
            }
        }

        /// <summary>
        /// 登录票据
        /// </summary>
        /// <param name="DtoModel"></param>
        public void SetAuthCookie(loginUserDataDtoModel DtoModel)
        {
            //将对象序列化
            var userdata = DtoModel.ToJson();
            //创建ticket村票据
            FormsAuthenticationTicket ticked = new FormsAuthenticationTicket(2, DtoModel.U_LoginName, DateTime.Now, DateTime.Now.AddDays(1), false, userdata);
            //进行加密票据
            var cookievalue = FormsAuthentication.Encrypt(ticked);
            //创建cookie
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cookievalue);
            cookie.HttpOnly = true;
            cookie.Expires = ticked.Expiration;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Domain = FormsAuthentication.CookieDomain;
            HttpContext context = HttpContext.Current;
            if (context == null)
            {
                throw new ArgumentNullException("context为空");

            }
            //先删除原有的
            HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            //添加cookie
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// 退出登录
        /// </summary>
        public void Logout()
        {
            //删除票据
            FormsAuthentication.SignOut();
            //清除cookie
            HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName].Expires = DateTime.Now.AddDays(-1);
            HttpContext.Current.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);

        }


    }
}
