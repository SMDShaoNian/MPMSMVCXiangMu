﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Common
{
    public class EnumTypes
    {

        /// <summary>
        /// 店铺类别
        /// </summary>
        public enum ShopTypeEnum
        {
            [Description("总部")]
            总部 = 1,
            [Description("加盟店")]
            加盟店 = 2,
            [Description("自营店")]
            自营店 = 3,

        }

        /// <summary>
        /// 用户角色
        /// </summary>
        public enum RoleTypeEnum
        {
            [Description("系统管理员")]
            系统管理员 = 1,
            [Description("店长")]
            店长 = 2,
            [Description("业务员")]
            业务员 = 3,

        }

        /// <summary>
        /// 会员卡状态
        /// </summary>
        public enum CardStateTypeEnum
        {
            [Description("正常")]
            正常 = 1,
            [Description("挂失")]
            挂失 = 2,
            [Description("锁定")]
            锁定 = 3,
        }

        /// <summary>
        /// 用户性别
        /// </summary>
        public enum SexTypeEnum
        {
            [Description("男")]
            男 = 1,
            [Description("女")]
            女 = 2,
        }

        /// <summary>
        /// 消费订单类型
        /// </summary>
        public enum ConsumeOrdersTypeEnum
        {
            [Description("兑换积分")]
            兑换积分 = 1,
            [Description("积分返现")]
            积分返现 = 2,
            [Description("减积分")]
            减积分 = 3,
            [Description("转介绍积分")]
            转介绍积分 = 4,
            [Description("快速消费")]
            快速消费 = 5,

        }


        public enum LeveType
        {
            [Description("普通会员")]
            普通会员 = 1,
            [Description("银卡会员")]
            银卡会员 = 2,
            [Description("金卡会员")]
            金卡会员 = 3,
            [Description("至尊会员")]
            至尊会员 = 4
        }

        public enum State
        {
            [Description("正常")]
            正常 = 1,
            [Description("已挂失")]
            已挂失 = 2,
            [Description("已锁定")]
            已锁定 = 3
        }
        public enum Sex
        {
            [Description("男")]
            男 = 1,
            [Description("女")]
            女 = 2,
        }



    }
}
