﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Common
{
    /// <summary>
    /// 定义一个枚举类型
    /// </summary>
    public enum ResultStatus
    {
        Error = -1,
        Success = 1,
    }
    public class OpearteResult
    {
        public ResultStatus resultstatus { get; set; }
        public string Msg { get; set; }
        public object data { get; set; }
        /// <summary>
        /// 有参数的构造函数
        /// </summary>
        /// <param name="status"></param>
        /// <param name="msg"></param>
        public OpearteResult(ResultStatus status, string msg)
        {
            this.resultstatus = status;
            this.Msg = msg;
        }
        public OpearteResult(ResultStatus status, string msg, object data)
        {
            this.resultstatus = status;
            this.Msg = msg;
            this.data = data;
        }



    }
}
