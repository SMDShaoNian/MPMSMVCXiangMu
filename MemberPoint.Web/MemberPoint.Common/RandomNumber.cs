﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Common
{
    public class RandomNumber
    {
        public static object _lock = new object();
        public static int count = 1;
        public static string GetRandom()
        {
            lock (_lock)
            {
                Random ran = new Random();
                return DateTime.Now.ToString("ddHHmmssfff") + ran.Next(0, 9).ToString();
            }
        }
    }
}
