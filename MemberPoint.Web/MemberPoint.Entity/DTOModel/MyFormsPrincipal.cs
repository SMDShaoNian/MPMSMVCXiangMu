﻿using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Security;
using System.Security.Principal;
using System.Web.Security;
using System;

namespace MemberPoint.Entity.DTOModel
{
    public class MyFormsPrincipal<T> : IPrincipal where T : class, new()
    {
        public IIdentity Identity
        {
            get;
            private set;
        }
        public T UserData
        {
            get;
            private set;
        }
        public MyFormsPrincipal(FormsAuthenticationTicket ticket, T UserData)
        {
            this.Identity = new FormsIdentity(ticket);
            this.UserData = UserData;
        }
        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }
    }
}
