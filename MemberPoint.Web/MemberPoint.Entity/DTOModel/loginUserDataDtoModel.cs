﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MemberPoint.Entity.DTOModel
{
    public class loginUserDataDtoModel
    {
        [Display(Name = "用户id")]
        public int U_ID { get; set; }
        [Display(Name = "用户登录名")]

        public string U_LoginName { get; set; }

        [Display(Name = "用户密码")]
        public string U_Password { get; set; }
    }
}
