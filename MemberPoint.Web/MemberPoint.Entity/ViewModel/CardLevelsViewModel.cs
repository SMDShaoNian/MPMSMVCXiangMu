﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
    public class CardLevelsViewModel
    {
        [DisplayName("等级编号")]
        public int CL_ID { get; set; }

        [Required]
        [DisplayName("等级名称")]
        public string CL_LevelName { get; set; }

        [Required]
        [DisplayName("所需最大积分")]
        public string CL_NeedPoint { get; set; }
        [DisplayName("扣分比例")]
        public double? CL_Point { get; set; }
        [DisplayName("折扣比例")]
        public double? CL_Percent { get; set; }
    }
}
