﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
   public class ConsumeOrdersViewModel
    {
        [Display(Name = "消费编号:")]
        public int CO_ID { get; set; }

        [Display(Name = "店铺编号:")]
        public int S_ID { get; set; }

        [Display(Name = "用户编号:")]
        public int? U_ID { get; set; }

        [Display(Name = "订单号:")]
        [StringLength(20)]
        public string CO_OrderCode { get; set; }

        [Display(Name = "订单类型:")]
        public byte CO_OrderType { get; set; }

        [Display(Name = "会员编号:")]
        public int? MC_ID { get; set; }

        [Display(Name = "会员卡号:")]
        [StringLength(50)]
        public string MC_CardID { get; set; }

        [Display(Name = "礼品编号:")]
        public int EG_ID { get; set; }

        [Display(Name = "额度:")]
        [Column(TypeName = "money")]
        public float? CO_TotalMoney { get; set; }

        [Display(Name = "实际支付:")]
        [Column(TypeName = "money")]
        
        public decimal CO_DiscountMoney { get; set; }

        [Display(Name = "兑/减积分:")]
        public int? CO_GavePoint { get; set; }

        [Display(Name = "积分返现:")]
        [Column(TypeName = "money")]
        public float? CO_Recash { get; set; }

        [Display(Name = "备注:")]
        [StringLength(255)]
        public string CO_Remark { get; set; }

        [Display(Name = "消费时间:")]
        public DateTime? CO_CreateTime { get; set; }

        [Display(Name = "会员姓名:")]
        [StringLength(20)]
        public string MC_Name { get; set; }

    }
}
