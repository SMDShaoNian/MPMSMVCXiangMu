﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
 public   class ExchangGiftsViewModel
    {

        [Key]
        [Display(Name = "礼品编号")]
        public int EG_ID { get; set; }

        [Display(Name = "店铺编号")]
        public int? S_ID { get; set; }

        [Display(Name = "礼品编码")]
        [Required]
        [StringLength(255)]
        public string EG_GiftCode { get; set; }

        [Display(Name = "礼品名称")]
        [Required]
        [StringLength(255)]
        public string EG_GiftName { get; set; }

        [Display(Name = "礼品图片")]
        [Required]
        [StringLength(255)]
        public string EG_Photo { get; set; }

        [Display(Name = "所需积分")]
        public int? EG_Point { get; set; }

        [Display(Name = "总数量")]
        public int? EG_Number { get; set; }

        [DisplayName("已兑换的数量")]
        public int? EG_ExchangNum { get; set; }

        [Display(Name = "备注")]
        [Required]
        [StringLength(255)]
        public string EG_Remark { get; set; }

    }
}
