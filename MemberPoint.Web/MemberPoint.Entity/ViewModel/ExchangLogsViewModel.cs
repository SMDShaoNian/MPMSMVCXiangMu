﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
  public  class ExchangLogsViewModel
    {
        [Display(Name = "兑换记录编号:")]
        public int EL_ID { get; set; }

        [Display(Name = "店铺编号:")]
        public int? S_ID { get; set; }

        [Display(Name = "用户编号:")]
        public int? U_ID { get; set; }

        [Display(Name = "会员编号:")]
 
        public int? MC_ID { get; set; }

        [Display(Name = "会员卡号:")]
        [Required]
        [StringLength(50)]
        public string MC_CardID { get; set; }

        [Display(Name = "会员姓名:")]
        [Required]
        [StringLength(20)]
        public string MC_Name { get; set; }

        [Display(Name = "礼品编号:")]
        public int? EG_ID { get; set; }

        [Display(Name = "礼品编码:")]
        [Required]
        [StringLength(50)]
        public string EG_GiftCode { get; set; }

        [Display(Name = "礼品名称:")]
        [Required]
        [StringLength(50)]
        public string EG_GiftName { get; set; }

        [Display(Name = "兑换数量:")]
        public int? EL_Number { get; set; }

        [Display(Name = "所用积分:")]
        public int? EL_Point { get; set; }

        [Display(Name = "兑换时间:")]
        public DateTime? EL_CreateTime { get; set; }



    }
}
