﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
  public  class GetPageConsumeOrdersViewModel
    {
        [Display(Name = "会员卡号:")]
      
        [StringLength(50)]
        public string MC_CardID { get; set; }

        [Display(Name = "兑/减积分:")]
        public int CO_GavePoint { get; set; }

        [Display(Name = "备注:")]
        public string CO_Remark { get; set; }

        [Display(Name = "当前页:")]
        public int page { get; set; }
        [Display(Name = "页的大小:")]
        public int rows { get; set; }



        [Display(Name = "开始日期：")]
        public string beginTime { get; set; }
        [Display(Name = "结束日期:")]
        public string EndTime { get; set; }
        [Display(Name = "订单编号:")]
        public string CO_OrderCode { get; set; }
        [Display(Name = "会员卡号:")]
        public string CardId { get; set; }
        [Display(Name = "符号:")]
        public string fuhao { get; set; }
        [Display(Name = "消费金额:")]
        public string money { get; set; }
        [Display(Name = "会员等级:")]
        public string CardLevel { get; set; }
        [Display(Name = "减去积分:")]
        public string GavePoint { get; set; }


    }
}
