﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
 public class GetPageExchangLogsViewModel
    {


        [Display(Name = "会员卡号:")]
        [StringLength(50)]
        public string MC_CardID { get; set; }

        [Display(Name = "兑换开始时间:")]
        public string startTime { get; set; }

        [Display(Name = "兑换结束时间:")]
        public string endTime { get; set; }

        [Display(Name = "当前页:")]
        public int page { get; set; }
        [Display(Name = "页的大小:")]
        public int rows { get; set; }



    }
}
