﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
    public class GetPageGetCardLevels
    {
        public int page { get; set; }
        public int rows { get; set; }

        public string name { get; set; }
        public string phone { get; set; }
    }
}
