﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
   public  class GetPageMemCardsViewModel
    {
        [Display(Name = "会员卡号:")]    
        [StringLength(50)]
        public string MC_CardID { get; set; }

        [Display(Name = "额度:")]
        [Column(TypeName = "money")]
        public float? CO_TotalMoney { get; set; }

        [Display(Name = "折扣比例:")]
        public float? CL_Percent { get; set; }

        [Display(Name = "兑/减积分:")]
        public int CO_GavePoint { get; set; }

        [Display(Name = "备注:")]
        public string CO_Remark { get; set; }

        [Display(Name = "当前页:")]
        public int page { get; set; }
        [Display(Name = "页的大小:")]
        public int rows { get; set; }
        
        [Display(Name = "会员名称")]
        public string MC_Name { get; set; }
        [Display(Name = "手机号")]
        public string MC_Mobile { get; set; }

        [Display(Name = "状态")]
        public int? MC_State { get; set; }
        [Display(Name = "等级")]
        public int? CL_ID { get; set; }

        [DisplayName("积分兑换比例")]
        public float? CL_Point { get; set; }
    }
}
