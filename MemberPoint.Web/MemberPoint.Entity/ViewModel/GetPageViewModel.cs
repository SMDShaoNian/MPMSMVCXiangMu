﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace MemberPoint.Entity.ViewModel
{
    public class GetPageViewModel
    {
        [Display(Name = "会员卡号")]
        public string MC_CardID { get; set; }
        [Display(Name = "会员名称")]
        public string MC_Name { get; set; }
        [Display(Name = "手机号")]
        public string MC_Mobile { get; set; }

        [Display(Name = "状态")]
        public int? MC_State { get; set; }
        [Display(Name = "等级")]
        public int? CL_ID { get; set; }
        [Display(Name = "页数")]
        public int page { get; set; }
        [Display(Name = "行数")]
        public int rows { get; set; }

    }
}