﻿using MemberPoint.Entity.POCOModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
   public class MemCardsViewModel
    {
        [Key]

        [Display(Name = "会员编号")]
        public int MC_ID { get; set; }
        
        

       public decimal CO_Recash { get; set; }
        public string CO_CreateTime { get; set; }
        

        public virtual CardLevels CardLevels { get; set; }

        public virtual Shops Shops { get; set; }

        [DisplayName("折扣比例")]
        public double? CL_Percent { get; set; }
        public string CL_LevelName { get; set; }

        public string MC_CreateTimeTo { get; set; }



        public float? CO_DiscountMoney { get; set; }


        [Display(Name = "会员等级")]
        public int? CL_ID { get; set; }

        [Display(Name = "店铺编号")]
        public int S_ID { get; set; }


        [Display(Name = "会员卡号")]
        public string MC_CardID { get; set; }


        [Display(Name = "卡片密码")]
        public string MC_Password { get; set; }

        [Display(Name = "确认密码")]
        public string PasswordConfirm { get; set; }

        [Display(Name = "旧密码")]
        public string OldPassword { get; set; }
        [Display(Name = "会员名称")]
        public string MC_Name { get; set; }


        [Display(Name = "会员性别")]
        public int? MC_Sex { get; set; }


        [Display(Name = "手机号码")]
        public string MC_Mobile { get; set; }

        [Display(Name = "靓照")]
        public string MC_Photo { get; set; }

        [Display(Name = "会员生日--月")]
        public int? MC_Birthday_Month { get; set; }

        [Display(Name = "会员生日--月")]
        public int? MC_Birthday_Day { get; set; }


        [Display(Name = "会员生日类型")]
        public bool MC_BirthdayType { get; set; }


        [Display(Name = "是否设置卡片过期")]
        public bool MC_IsPast { get; set; }

        [Display(Name = "卡片过期时间")]
        public DateTime? MC_PastTime { get; set; }

        [Display(Name = "当前积分")]
        public int? MC_Point { get; set; }

        [Display(Name = "卡片付费")]
        public float? MC_Money { get; set; }

        [Display(Name = "累计消费")]
        public float? MC_TotalMoney { get; set; }

        [Display(Name = "累计消费次数")]
        public int MC_TotalCount { get; set; }

        [Display(Name = "卡片状态")]
        public int? MC_State { get; set; }

          [Display(Name = "累计积分数量")]
        public int CL_PointTo { get; set; }

        [DisplayName("积分兑换比例")]
        public double? CL_Point { get; set; }

        [Display(Name = "积分是否可以自动换成等级")]
        public bool MC_IsPointAuto { get; set; }

        [Display(Name = "推荐人ID")]
        public int MC_RefererID { get; set; }

        [Display(Name = "推荐人卡号")]
        public string MC_RefererCard { get; set; }

        [Display(Name = "推荐人姓名")]
        public string MC_RefererName { get; set; }

        [Display(Name = "登记日期")]
        public DateTime? MC_CreateTime { get; set; }

        [Display(Name = "会员等级")]
        public string LeveType { get; set; }
        [Display(Name = "枚举性别")]
        public string EnumSex { get; set; }
        [Display(Name = "枚举状态")]
        public string EnumState { get; set; }

    }
}
