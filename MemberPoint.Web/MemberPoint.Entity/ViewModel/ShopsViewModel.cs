﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
    public class ShopsViewModel
    {

        [DisplayName("店铺编号")]
        public int S_ID { get; set; }


        [DisplayName("店铺名称")]
        public string S_Name { get; set; }


        [DisplayName("店铺类别")]
        public int? S_Category { get; set; }


        [DisplayName("联系人")]
        public string S_ContactName { get; set; }


        [DisplayName("联系电话")]
        public string S_ContactTel { get; set; }


        [DisplayName("地址")]
        public string S_Address { get; set; }


        [DisplayName("备注")]
        public string S_Remark { get; set; }


        [DisplayName("是否已分配管理员")]
        public bool? S_IsHasSetAdmin { get; set; }//非0即真


        [DisplayName("加盟时间")]
        public DateTime? S_CreateTime { get; set; }

    }
}
