﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
    public class TransferViewModel
    {
        [Display(Name = "用户id")]
        public int UserId { get; set; }
        [Display(Name = "转出会员卡号")]
        public string Out_MC_CardID { get; set; }
        [Display(Name = "转出会员姓名")]
        public string Out_MC_Name { get; set; }
        [Display(Name = "转出会员积分")]
        public int? Out_MC_Point { get; set; }
        [Display(Name = "转出会员消费")]
        public float? Out_MC_TotalMoney { get; set; }
        [Display(Name = "余额")]
        public float? MC_Money { get; set; }

        [Display(Name = "转入会员卡号")]
        [Required(ErrorMessage = "不能为空")]
        public string In_MC_CardID { get; set; }
        [Display(Name = "转入会员姓名")]
        [Required(ErrorMessage = "不能为空")]
        public string In_MC_Name { get; set; }
        [Display(Name = "转入会员积分")]
        [Required(ErrorMessage = "不能为空")]
        public int? In_MC_Point { get; set; }
        [Display(Name = "转入会员消费")]
        [Required(ErrorMessage = "不能为空")]
        public float? In_MC_TotalMoney { get; set; }

        [Display(Name = "转账金额")]
        [Required(ErrorMessage = "不能为空")]
        public int TransferMoney { get; set; }

        [Display(Name = "备注")]
        public string Remark { get; set; }
    }
}
