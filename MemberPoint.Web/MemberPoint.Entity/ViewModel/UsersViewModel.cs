﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MemberPoint.Entity.ViewModel
{
   public class UsersViewModel
    {
        [Display(Name = "用户编号")]
        public int U_ID { get; set; }

        [Display(Name = "店铺编号")]
        public int S_ID { get; set; }

        [Display(Name = "店铺名")]
      
        public string S_Name { get; set; }

        [Display(Name = "用户登录名")]
     
        public string U_LoginName { get; set; }

        [Display(Name = "密码")]
 
        public string U_Password { get; set; }


        [Display(Name = "真实姓名")]
     
        public string U_RealName { get; set; }

        [Display(Name = "性别")]
       
        public string U_Sex { get; set; }
        [Display(Name = "电话")]
      
        public string U_Telephone { get; set; }
        [Display(Name = "角色")]

        public int U_Role { get; set; }

        [Display(Name = "角色")]
        public string U_RoleName { get; set; }
        [Display(Name = "是否可以删除")]
        public bool U_CanDelete { get; set; }

        


    }
}
