﻿using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.POCOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MemberPoint.Web.Controllers
{
    public class CardLevelsController : Controller
    {



        protected CardLevelsService levels = new CardLevelsService();


        /// <summary>
        /// 会员等级列表
        /// </summary>
        /// <returns></returns>
        public ActionResult CardLevelsInfo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CardLevelsInfo(GetPageGetCardLevels getgage)
        {
            var result = levels.GetCardLevelsList(getgage);
            return Json(result.data, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 新增会员卡类别
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var aa = new CardLevelsViewModel();
            return View(aa);
        }
        [HttpPost]
        public ActionResult Create(CardLevelsViewModel info)
        {
            var result = levels.AddCardLevels(info);
            if (result.resultstatus == ResultStatus.Success)
            {
                return Json(result);
            }
            else
            {
                return Content("添加失败");
            }

        }
        /// <summary>
        /// 修改会员卡类别
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            var result = levels.ShowCardLevels(id);
            return View("Create", result);
        }
        [HttpPost]
        public ActionResult Edit(CardLevelsViewModel info, int id)
        {
            var result = levels.EditCardLevels(info, id);
            if (result.resultstatus == ResultStatus.Success)
            {
                return Json(result);
            }
            else
            {
                return Content("修改失败");
            }

        }
        /// <summary>
        /// 删除会员卡类别
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            var query = levels.DeleteCardLevels(id);
            if (query.resultstatus == ResultStatus.Success)
            {
                return Json(query);
            }
            else
            {
                return Content("删除失败");
            }
        }


    }
}