﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.DTOModel;

namespace MemberPoint.Web.Controllers
{
    public class ConsumeOrderController : Controller
    {
        private ConsumeOrdersService consume = new ConsumeOrdersService();
        // GET: ConsumeOrder

        #region 消费历史记录
        public ActionResult ConsumeOrdersList()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ConsumeOrdersList(GetPageConsumeOrdersViewModel consumeorder)
        {
            var list = consume.GetConsumeOrdersList(consumeorder);
            return Json(list.data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 消费历史记录统计
        public ActionResult ConsumeOrdersListAll()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ConsumeOrdersListAll(GetPageConsumeOrdersViewModel consumeorder)
        {
            var list = consume.GetConsumeOrdersList(consumeorder);
            return Json(list.data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region 马上结算
        /// <summary>
        /// 马上结算
        /// </summary>
        /// <returns></returns>
        public ActionResult GotoBalance()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GotoBalance(GetPageMemCardsViewModel getMember)
        {
            var user = User as MyFormsPrincipal<loginUserDataDtoModel>;
            var id = user.UserData.U_ID;
            var result = consume.GotoBalance(getMember,id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 会员减积分（马上扣除）
        public ActionResult DeductMemberPoint()
        {
            return View();
        }

        /// <summary>
        /// 马上扣除【操作ConsumeOrders表】
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeductMemberPoint(GetPageMemCardsViewModel getConsumeOrders)
        {
            try
            {
                var user = User as MyFormsPrincipal<loginUserDataDtoModel>;
                var id = user.UserData.U_ID;

                var result = consume.DeductMemberPoint(getConsumeOrders,id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
          
          
        }


        #endregion

        #region 马上返现
        public ActionResult GoTORecash()
        {
            return View();
        }
        /// <summary>
        /// 马上返现【操作ConsumeOrders表】
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GoTORecash(GetPageMemCardsViewModel getMemCards)
        {
            try
            {
                var result = consume.GoTORecash(getMemCards);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        #endregion
        
        protected CardLevelsService level = new CardLevelsService();

        #region 积分返现统计 快速消费统计 获取消费记录中的信息
        /// <summary>
        /// 积分返现统计 快速消费统计 获取消费记录中的信息
        /// </summary>
        /// <returns></returns>
        public ActionResult ConsumeOrderInfo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ConsumeOrderInfo(GetPageConsumeOrdersViewModel getpage)
        {

            var result = consume.GetConsumeOrderList(getpage);
            return Json(result.data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 减积分统计  获取消费记录中的信息
        /// <summary>
        ///减积分统计  获取消费记录中的信息
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowConsumeOrderInfo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ShowConsumeOrderInfo(GetPageConsumeOrdersViewModel getpage)
        {

            var result = consume.GetConsumeOrderList(getpage);
            return Json(result.data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 会员等级下拉框的绑定
        [HttpPost]
        public ActionResult BandcardLevels()
        {
            var result = level.GetCardLevels();
            return Json(result.data);
        }
        #endregion


    }
}