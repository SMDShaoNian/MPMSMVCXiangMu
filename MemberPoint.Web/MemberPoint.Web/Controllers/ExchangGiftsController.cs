﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;

namespace MemberPoint.Web.Controllers
{
    public class ExchangGiftsController : Controller
    {
        private ExchangeGiftsService exchangGifts = new ExchangeGiftsService();

        // GET: ExchangGifts
        #region  修改兑换数量
        /// <summary>
        /// 修改兑换数量
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EG_ExchangNumEdit(int id)
        {
            var change = exchangGifts.GetExchangGiftsEG_ID(id);
            return View(change.data);
        }
        [HttpPost]
        public ActionResult EG_ExchangNumEdit(ExchangGiftsViewModel getGifts)
        {
            var list = exchangGifts.EditEG_ExchangNum(getGifts);
            return Json(list, JsonRequestBehavior.AllowGet);

        }




        #endregion

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        // GET: ExchangGifts
        /// <summary>
        /// 礼品列表
        /// </summary>
        /// <returns></returns>

        public ActionResult Index(PageFenYe pageDate)
        {
            OpearteResult result = exchangGifts.GetExchangGifts(pageDate);
            return Json(result.data);
        }
        public ActionResult Create()
        {
            return View();
        }
        /// <summary>
        /// 添加礼品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]

        public ActionResult Create(ExchangGiftsViewModel exchang)
        {
            OpearteResult result;
            try
            {
                result = exchangGifts.AddExchang(exchang);
                if (result.resultstatus == ResultStatus.Success)
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result);
                }

            }
            catch
            {
                return View();
            }
        }
        /// <summary>
        /// 编辑
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            //调用userService的方法
            OpearteResult result = exchangGifts.GetExchangFirst(id);
            return View("Create", result.data);
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="exchang"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(ExchangGiftsViewModel exchang, int id)
        {
            OpearteResult result;
            try
            {
                result = exchangGifts.UpdateExchang(exchang, id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            //调用userService的方法
            OpearteResult result = exchangGifts.DeleteExchang(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}