﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.DTOModel;

namespace MemberPoint.Web.Controllers
{
    public class ExchangLogsController : Controller
    {
        private ExchangLogsService exchange = new ExchangLogsService();
        

        // GET: ExchangLogs
        #region 兑换历史记录
        public ActionResult ExchangLogsHistoryList()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ExchangLogsHistoryList(GetPageExchangLogsViewModel change)
        {
            var changes = exchange.GetExchangLogsInfo(change);
            return Json(changes.data, JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region 礼品兑换统计
        public ActionResult ExchangLogsList()
        {

            return View();
        }

        [HttpPost]
        public ActionResult ExchangLogsList(GetPageExchangLogsViewModel change)
        {
            var changes = exchange.GetExchangLogsInfo(change);
            return Json(changes.data, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region 马上兑换
        /// <summary>
        /// 马上兑换
        /// </summary>
        /// <param name="shuju"></param>
        /// <returns></returns>
    

        public ActionResult ComeConversion(/*string id,string MC_CardID*/)
        {
            //return Content(id,MC_CardID);
           var id= Request.QueryString["str"];
            var MC_CardID = Request.QueryString["mc_id"];
            var user = User as MyFormsPrincipal<loginUserDataDtoModel>;
            var userid = user.UserData.U_ID;
            var date = exchange.ComeConversion(id,MC_CardID, userid);

            if (date.resultstatus == ResultStatus.Success)
            {
                return RedirectToAction("PointChangeGifts", "MemCards");
            }
            else
            {
                return View();
            }


        }


        #endregion







    }
}