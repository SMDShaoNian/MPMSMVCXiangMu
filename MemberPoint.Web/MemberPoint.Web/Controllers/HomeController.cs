﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;


namespace MemberPoint.Web.Controllers
{
    public class HomeController : Controller
    {

        private UserService us = new UserService();

        [Authorize]
        public ActionResult Index()
        {
            return View();
        }



        //登陆
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UsersViewModel uvm)
        {
            var date = us.Login(uvm);
            return Json(date);
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}