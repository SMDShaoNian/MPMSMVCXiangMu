﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Entity.DTOModel;
using System.Data;

namespace MemberPoint.Web.Controllers
{
    public class MemCardsController : Controller
    {

        private  MemCardsService member = new MemCardsService();
        private ExchangeGiftsService exchange = new ExchangeGiftsService();
        // GET: MemCards

        #region 查找会员
        public ActionResult FindMenberCard()
        {
            return View();
        }
        /// <summary>
        /// 查找会员
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FindMenberCard(GetPageMemCardsViewModel getMember)
        {
            var list = member.FindMemCards(getMember);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 积分兑换礼品
        //操作礼品表(ExchangGifts)
        public ActionResult PointChangeGifts()
        {
            return View();

        }
        [HttpPost]
        public ActionResult PointChangeGifts(GetPageMemCardsViewModel getMember)
        {
            var list = exchange.PointChangeGifts(getMember);
            return Json(list.data, JsonRequestBehavior.AllowGet);
        }

        #endregion


        private MemCardsService memcards = new MemCardsService();
        #region 会员展示
        /// <summary>
        /// 展示会员信息
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 获取用户数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult GetMemCardsList(GetPageViewModel model)
        {
            OpearteResult result;
            //获取当前用户所在店铺
            BaseService<MemberPoint.Entity.POCOModel.Users> UserBase = new BaseService<Entity.POCOModel.Users>();
            var user = User as MyFormsPrincipal<loginUserDataDtoModel>;
            result = memcards.GetMemCardsList(model, UserBase.Find(u => u.U_ID == user.UserData.U_ID).S_ID);
            return Json(result.data, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region 添加会员信息
        /// <summary>
        /// 添加会员
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            MemCardsViewModel m = new MemCardsViewModel();
            return View(m);
        }
        [HttpPost]
        public ActionResult Create(MemCardsViewModel modelinfo)
        {
            try
            {
                OpearteResult reselt;
                reselt = memcards.AddInfo(modelinfo);

                return Json(reselt);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// 查询姓名是否存在
        /// </summary>
        /// <param name="mName"></param>
        /// <returns></returns>
        public ActionResult selName(string mName)
        {
            OpearteResult result;
            if (memcards.Find(m => m.MC_Name == mName) != null)
            {
                result = new OpearteResult(ResultStatus.Success, "推荐人存在");
            }
            else
            {
                result = new OpearteResult(ResultStatus.Error, "推荐人不存在");
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region 修改会员信息
        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <returns></returns>
        public ActionResult Edit(string id)
        {

            try
            {
                OpearteResult result = memcards.Getfirst(id);

                return View("Create", result.data);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        [HttpPost]
        public ActionResult Edit(MemCardsViewModel model)
        {
            try
            {
                OpearteResult reselt;
                reselt = memcards.UpdateMemCards(model);
                return Json(reselt);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region 删除会员信息
        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        public ActionResult Delete(string id)
        {
            OpearteResult result = memcards.DeleteMemCards(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 挂失锁定
        /// <summary>
        /// 挂失/锁定
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Lock(string id)
        {
            MemCardsViewModel m = new MemCardsViewModel()
            {
                MC_CardID = id
            };
            return View(m);
        }
        [HttpPost]
        public ActionResult Lock(MemCardsViewModel model)
        {
            try
            {
                OpearteResult result = memcards.LockMemCards(model);
                return Json(result);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region 会员转账
        public ActionResult VIPTransfer(string id)
        {
            var mem = memcards.Getfirst(id).data as MemCardsViewModel;
            TransferViewModel transfer = new TransferViewModel
            {
                Out_MC_CardID = mem.MC_CardID,
                Out_MC_Name = mem.MC_Name,
                Out_MC_Point = mem.MC_Point,
                Out_MC_TotalMoney = mem.MC_TotalMoney,
                MC_Money = mem.MC_Money
            };
            return View(transfer);
        }
        /// <summary>
        /// 执行转账
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VIPTransfer(TransferViewModel model)
        {
            OpearteResult result;
            if (ModelState.IsValid)
            {
                var users = User as MyFormsPrincipal<loginUserDataDtoModel>;
                model.UserId = users.UserData.U_ID;
                result = memcards.VipTransfer(model);

            }
            else
            {
                result = new OpearteResult(ResultStatus.Error, "不能为空");
            }
            return Json(result);
        }

        /// <summary>
        /// 根据当前用户输入的卡号查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult VIPInto(string id)
        {
            OpearteResult result = memcards.Getfirst(id);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region 会员换卡
        /// <summary>
        /// VIP换卡
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult VIPReset(string id)
        {
            OpearteResult result = memcards.ResetGetBycard(id);
            return View(result.data);
        }


        /// <summary>
        /// 换卡事件
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VIPReset(MemCardsViewModel model)
        {
            OpearteResult result = memcards.UpdateCards(model);
            return Json(result);
        }
        #endregion

        #region 导出Excel

        /// <summary>
        /// 导出Excel
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportExcelNew()
        {
            string json = Request.Params["data"];
            try
            {
                DataTable dt = ExcelHelper.JsonToDataTable(json);
                string pathDestop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ExcelHelper.GridToExcelByNPOI(dt, pathDestop + "\\" + "会员列表-" + DateTime.Now.ToString("yyyy-MM-dd") + "导出" + ".xls");
                return Content("1");
            }
            catch (Exception)
            {
                return Content("-1");
            }
        }
        #endregion


    }
}