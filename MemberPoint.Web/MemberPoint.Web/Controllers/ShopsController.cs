﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;
using MemberPoint.Common;


namespace MemberPoint.Web.Controllers
{
    public class ShopsController : Controller
    {
        // GET: Shops

        private ShopsService ss = new ShopsService();
        /// <summary>
        ///  展示店铺信息
        /// </summary>
        /// <returns></returns>


        public ActionResult ShowShopInfo()
        {
            return View();
        }

        [HttpPost]

        public ActionResult ShowShopInfo(int page, int rows, string Sname, string SContactName, string adress)
        {
            var date = ss.GetInfo(page, rows, Sname, SContactName, adress);
            return Json(date.data);
        }
        public ActionResult BindShopsType()
        {
            //var list = db.GoodsType.Select(a => new SelectListItem { Text = a.CName, Value = a.Cid.ToString() });
            List<SelectListItem> list = new List<SelectListItem>();
            foreach (int i in Enum.GetValues(typeof(EnumTypes.ShopTypeEnum)))
            {
                SelectListItem listitem = new SelectListItem() { Text = Enum.GetName(typeof(EnumTypes.ShopTypeEnum), i), Value = i.ToString() };
                list.Add(listitem);
            }
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 添加店铺
        /// </summary>
        /// <returns></returns>
        public ActionResult AddShops()
        {
            //ViewBag.type=BindShopsType();
            return View();
        }

        [HttpPost]
        public ActionResult AddShops(ShopsViewModel uvm)
        {

            //SelectListItem listitem = new SelectListItem();
            //uvm.S_Category =Convert.ToInt32(listitem.Value);
            //ViewBag.type = BindShopsType();
            var a = ss.AddShops(uvm);
            return Json(a);
        }
        /// <summary>
        /// 删除店铺
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //[HttpPost]
        public ActionResult DeleteInfo(int Id)
        {

            var a = ss.DeleteInfo(Id);
            return Json(a);

        }
        /// <summary>
        /// 修改店铺信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EditShop(int id)
        {
            var a = ss.GetShopsById(id);
            return View("AddShops", a.data);
        }

        [HttpPost]
        public ActionResult EditShop(ShopsViewModel uvm, int id)
        {
            var result = ss.EditShop(uvm, id);
            return Json(result);
        }
        /// <summary>
        /// 分配店长账号
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Assigned(int id)
        {
            UsersViewModel mo = new UsersViewModel();
            mo.S_ID = id;
            return View(mo);
        }
        [HttpPost]
        public ActionResult Assigned(UsersViewModel mo)
        {

            UserService us = new UserService();
            var a = us.AddShopManager(mo);
            return Json(a);

        }


    }
}