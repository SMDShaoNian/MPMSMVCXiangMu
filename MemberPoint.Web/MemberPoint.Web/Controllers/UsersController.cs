﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MemberPoint.Business;
using MemberPoint.Common;
using MemberPoint.Entity.POCOModel;
using MemberPoint.Entity.ViewModel;

namespace MemberPoint.Web.Controllers
{
    public class UsersController : Controller
    {

        private UserService us = new UserService();


        /// <summary>
        /// 下拉框
        /// </summary>
        /// <param name="shops"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UsersList()
        {
            ShopsService ss = new ShopsService();
            var date = ss.ShopsList();
            return Json(date.data);
        }

        /// <summary>
        ///  展示用户信息
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowUsersInfo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ShowUsersInfo(int page, int rows,string sid,string uname)
        {
            var date = us.GetInfo(page,rows,sid,uname);
            return Json(date.data);
        }
        
        public ActionResult AddUsersInfo()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddUsersInfo(UsersViewModel uvm)
        {
            var a = us.AddUser(uvm);
            return Json(a);
        }

        public ActionResult UpdateUsersInfo(int id)
        {
            var a = us.GetUsersById(id);
            return View(a.data);
        }

        [HttpPost]
        public ActionResult UpdateUsersInfo(UsersViewModel uvm,int id)
        {
            var result = us.EditUser(uvm,id);
            return Json(result);
        }


        [HttpPost]
        public ActionResult DeleteInfo(int id)
        {
            var a = us.DeleteInfo(id);
            return Json(a);

        }



    }
}