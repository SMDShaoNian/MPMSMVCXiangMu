﻿using MemberPoint.Common;
using MemberPoint.Entity.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace MemberPoint.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        //验证身份
        protected void Application_PostAuthenticateRequest(object sender, System.EventArgs e)
        {

            HttpApplication app = sender as HttpApplication;
            HttpContext context = app.Context;
            //读取cookie
            var cookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie != null)
            {
                //解密cookie
                var Ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (!string.IsNullOrWhiteSpace(Ticket.UserData))
                {
                    var dtomodel = Ticket.UserData.Toobject<loginUserDataDtoModel>();
                    context.User = new MyFormsPrincipal<loginUserDataDtoModel>(Ticket, dtomodel);
                }
            }


        }


    }
}
