﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MemberPoint.Web.Startup))]
namespace MemberPoint.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
